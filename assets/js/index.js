document.addEventListener('DOMContentLoaded', () => {
    let url = '';
    let nameField = document.getElementById('name');
    if(nameField != null) {
        let div = document.getElementById('users');
        let ul = document.createElement('ul');
        div.append(ul);
        nameField.addEventListener('keyup', () => {
            if(nameField.value.length >= 3) {
                url = 'service.sql?max=10&str=' + nameField.value;
                fetch(url)
                    .then((resp) => resp.json())
                    .then((resp) => {
                        ul.innerHTML = '';
                        resp.data.map( (user) => {
                            let li = document.createElement('li');
                            let span = document.createElement('span');
                            span.innerHTML = `${user.last_name} ${user.first_name} - ${user.email}`;
                            li.append(span);
                            ul.append(li);
                        });
                    })
                    .catch( function(error) {
                        console.log(error);
                    });
            } else {
                ul.innerHTML = '';
            }
        });
    }
});

// <tr><td class="name align-middle">aaab</td></tr>
