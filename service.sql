select
    'json' as component,
    json_object(
        'data',( 
            select json_group_array(
                json_object(
                    'first_name',first_name,
                    'last_name',last_name,
                    'email',email
                )
            ) 
            from users 
            where 
                last_name like $str || '%' 
                limit $max)
    ) as contents;

